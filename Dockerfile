FROM ruimashita/pip

RUN pip install docker-compose

ENTRYPOINT ["docker-compose"]